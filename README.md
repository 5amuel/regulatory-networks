# Regulatory Networks in Trees
This study uses known TF motif to build regulatory networks. The basic idea is to predict TF gene interactions iff there is a significant TFBS in the promoter of the gene and the expression profile of the TF and gene, allows for regulatory relationship.

## git::CHEATSHEET 

#### Issue, Branch, Merge request:

https://gitlab.com/5amuel/regulatory-networks/-/issues/new
Create Issue Reduce repeatative code #19 on gitlab ☝️
Add [] (thick of boxes for tasks/part of the code to maintain)
[] Data collection 
[] Correlation btw TF and central gene
git branch issue_19 (git branch show local branches, git branch -a show local + remote b)
git checkout issue_19 (In short we can write “git checkout -b issue_#”)
git status
git add . (git add filename, stage a file to the staging area, enabling commit -m for each file)
git reset filename (removes the file from the staging area )
git commit -m "comment"
git push -u origin issue_19 (sets up a remote issue_19 branch that can be pushed directly to by: git push issue_19)
git status 

### Merge the issue into the master branch:
git status  
git commit -m “.., closes #19” (The #19, connect with the remote issue)
git branch --merged (show merged branches)
git checkout master
git merge issue_19 (merges the branch to master and closing the issue) 
git push (merge and push can be done in one step by “git push origin issue_19” which create merge request right away)

### Overveiw of branches and merges
git log --online --graph

### undo git merge to master
https://www.youtube.com/watch?v=_O6uANApwH0

### Rebase
git rebase -i HEAD~3

### Examples:
git commit -m "Changed the BLASTp query to contain plant_uniprot_ids instead of a plant_fastafile with protein sequences. Along with other changes contributing to seamless creation of a Regulatory Network in Aspen, Closes Issue_4"

git merge issue_4
Merge branch 'issue_4'
This merge includes changes related to updating the BLASTp query to contain plant_uniprot_ids instead of a plant_fastafile. It also includes various other modifications contributing to the seamless creation of a Regulatory Network in Aspen. Closes Issue_4.

## r::CHEATSHEET:
%>% (Cmd + Shift + M) 


## Starship::CHEATSHEET:
[directory]
style = "bold cyan"

[character]
success_symbol = "[❯](bold green)"
error_symbol = "[✘](bold red)"
vimcmd_symbol = "[❯](bold yellow)"

[git_branch]
symbol = '𝞇'  ⦿⦿
style = "bold cyan"
### git_bramch show "on" deafault -> repo > on 𝞇 master [+1]
format = "on [$branch$symbol]($style) 𝞇 " # format remove "on" -> repo > 𝞇 master[+1]

[git_commit]
tag_symbol = '🔖 '

[git_status]
ahead = '+${count}'   
behind = '-${count}' 
diverged = '⇕+${ahead_count}-${behind_count}'
up_to_date = "[✓](bold green)"
untracked = '🤷 '
stashed = '📦 '
modified = '📝 '
staged = '[++\($count\)](green)'
deleted = '🗑'

[git_state]
cherry_pick = '[🍒 PICKING](bold red)'

[jobs]
symbol = '+ '
number_threshold = 4
symbol_threshold = 1

[hostname]
ssh_only = true
style = "bold cyan"

### Get editor completions based on the config schema
"$schema" = 'https://starship.rs/config-schema.json'



